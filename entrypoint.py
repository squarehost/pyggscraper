import formatters, scraper

import argparse, logging, sys, time, traceback

EXIT_CODE_INVALID_ARGS = 10
EXIT_CODE_INVALID_INIT = 20
EXIT_CODE_SCRAPE_ERROR = 30
EXIT_CODE_FORMATTER_ERROR = 40
EXIT_CODE_INVALID_OUTPUT_FILE = 50

parser = argparse.ArgumentParser(
    description='Scrape campaign information from IndieGoGo APIs'
)
parser.add_argument(
    "--type", dest="type", action="store",
    default="pledges",
    help="The type of data to scrape. Choose from {}".format(
        ", ".join(scraper.all_types.keys( ))
    )
)
parser.add_argument(
    "--debug", dest="debug", action="store_true",
    help="Display verbose information for debugging."
)
parser.add_argument(
    "--field", dest="fields", action="append", type=str,
    help=(
        "Include this field within the data payload. Can be supplied "
        "multiple times to include multiple field. Default is to use "
        "useful data. Use debug mode to print all possible fields."
    )
)
parser.add_argument(
    "--output-file", dest="output_file", action="store", type=str,
    help=(
        "Output to this file name. Default is to use <campaign_id>.<format>"
    )
)
parser.add_argument(
    "--stdout", dest="stout", action="store_true",
    help=(
        "Output to stdout rather than a file."
    )
)
parser.add_argument(
    "--filter-below", dest="below_amount", action="store", type=int,
    help=(
        "Filter out pledges below this amount (this should be an integer "
        "number and doesn't take into consideration different currencies)."
    )
)
parser.add_argument(
    "--sort", dest="sort", action="store", type=str,
    help=(
        "Sort by this field. Default is to sort by reversed 'natural' order. "
        "E.g. the order IGG gives us in the api."
    )
)
parser.add_argument(
    "--sort-desc", dest="desc", action="store_true",
    help=(
        "Sort in descending order. Default is ascending."
    )
)
parser.add_argument(
    "--formatter", dest="formatter", action="store", type=str,
    default="csv", help=(
        "Select a format to output the scraped data into. "
        "Choose from {}".format(
            ", ".join(formatters.all_formatters.keys( ))
        )+(
            ". Data will be output to stdout, so can be stored by "
            "redirecting to a file name."
        )
    )
)
parser.add_argument(
    "campaign_id", type=int, help='The IGG unique campaign ID.'
)

if __name__ == "__main__":
    args = parser.parse_args( )

    all_types = list(scraper.all_types.keys( ))
    all_types.sort( )
    all_types = ", ".join([f"'{t}'" for t in all_types])
    all_formatters= list(formatters.all_formatters.keys( ))
    all_formatters.sort( )
    all_formatters = ", ".join([f"'{t}'" for t in all_formatters])

    debug = args.debug

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG, stream=sys.stderr
        )
    else:
        logging.basicConfig(
            level=logging.INFO, stream=sys.stderr
        )

    logger = logging.getLogger("pyGGScraper")

    if args.type not in scraper.all_types:
        logger.error(
            f"Invalid data type {args.type}, choose from "
            f"{all_types}"
        )
        sys.exit(EXIT_CODE_INVALID_ARGS)

    if args.formatter not in formatters.all_formatters:
        logger.error(
            f"Invalid output format {args.formatter}, choose from "
            f"{all_formatters}"
        )
        sys.exit(EXIT_CODE_INVALID_ARGS)


    sort = None
    type = scraper.all_types[args.type]
    formatter = formatters.all_formatters[args.formatter]
    outf = args.output_file or "{}.{}".format(
        args.campaign_id, args.formatter
    )
    if not args.stout:
        try:
            with open(outf, "a") as fh:
                pass
        except Exception as ex:
            logger.error(
                f"Unable to open output file {outf}: {ex}"
            )
            sys.exit(EXIT_CODE_INVALID_OUTPUT_FILE)

    kwargs = {
        'campaign_id': args.campaign_id,
    }
    if args.fields: kwargs['fields'] = args.fields
    if args.sort: kwargs['sort'] = "{}{}".format(
        "-" if args.desc else "", args.sort
    )
    if args.below_amount: kwargs['filter_amount'] = args.below_amount

    logger.debug(
        f"Attempting to instantiate scraper {type} with kwargs {kwargs}"
    )
    try:
        klass = type(**kwargs)
    except Exception as ex:
        if debug:
            logger.error(traceback.format_exc( ))
        else:
            logger.error(ex)
        sys.exit(EXIT_CODE_INVALID_INIT)
        logger.error(
            "Unable to create scraper with these options. Please revise and "
            "try again."
        )

    logger.info(
        f"Scraping '{type}' information for campaign {kwargs['campaign_id']}"
    )
    st = time.time( )
    try:
        data = klass.all_data
    except Exception as ex:
        if debug:
            logger.error(traceback.format_exc( ))
        else:
            logger.error(ex)
        logger.error(
            "An error occurred when trying to scrape this data. Please "
            "try again, or if the problem persists, try different options."
        )
        sys.exit(EXIT_CODE_SCRAPE_ERROR)

    et = round(time.time( ) - st, 2)
    logger.info(
        f"Data scrape completed in {et} seconds. "
        f"{len(data)} total record(s) retrieved."
    )
    logger.info(
        f"Outputting records into '{args.formatter}' format..."
    )
    try:
        dt = "{}\n".format(
            formatter(data, klass.fields, args.campaign_id)
        )
        if args.stout:
            sys.stdout.write(dt)
        else:
            with open(outf, "w") as fh:
                fh.write(dt)
    except Exception as ex:
        if debug:
            logger.error(traceback.format_exc( ))
        else:
            logger.error(ex)
        logger.error(
            "An error occurred when trying to format this data. Please "
            "try again, or if the problem persists, try a different format."
        )
        sys.exit(EXIT_CODE_FORMATTER_ERROR)
