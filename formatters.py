import datetime, copy, csv, io

BASE_HTML_TEMPLATE = '''
<!doctype html>
<html>
    <head>
        <title>Scraped data for IGG Campaign {campaign_id}</title>
    </head>
    <body>
        <h2>Data obtained at {time}</h2>
        {table}
    </body>
</html>
'''

def csv_format(lst, fields, campaign_id):
    if not lst: return ""

    output = io.StringIO( )
    w = csv.writer(output, lineterminator="\n")
    w.writerow(["Count"] + fields)
    for i, item in enumerate(lst):
        dt = [str(i+1)]
        for f in fields:
            dt.append(item.get(f) or '')
        w.writerow(dt)
    return output.getvalue()

def html_format(lst, fields, campaign_id, template=BASE_HTML_TEMPLATE):
    if not lst: return ""

    base = copy.copy(template)
    header = ["Count"] + [" ".join(f.split("_")).title( ) for f in fields]
    rows = []
    for i, item in enumerate(lst):
        dt = ["<td>{}</td>".format(i+1)]
        for f in fields:
            dt.append("<td>{}</td>".format(
                item.get(f) or ''
            ))
        rows.append("".join(dt))
    
    table = '''
    <table style="width:100%;">
        <thead><tr>
            {head}
        </tr></thead>
        <tbody>
            {rows}
        </tbody>
    </table>
    '''.format(
        head="\n".join(["<th>{}</th>".format(h) for h in header]),
        rows="\n".join(["<tr>{}</tr>".format(r) for r in rows])
    )
    return template.format(
        campaign_id=campaign_id,
        table=table,
        time=datetime.datetime.now( ).strftime("%Y-%m-%d %H:%M:%S"),
    )


all_formatters = {
    "csv": csv_format,
    "html": html_format
}