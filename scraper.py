import dateparser, requests

import logging, re, time

INT_REGEX = re.compile("[^0-9\.]")

IGG_BASE_URI = "https://www.indiegogo.com/private_api/"

IGG_API_URLS = {
    "pledges": f"{IGG_BASE_URI}campaigns/{{campaign_id}}/pledges/"
}

DEFAULT_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
    'Accept': 'application/json'
}

INITIAL_RATE_LIMIT = 5

class IGGPageScraper(object):
    rate_limit_delay_seconds = INITIAL_RATE_LIMIT
    def __init__(self, type, **kwargs):
        self.url = IGG_API_URLS[type].format(**kwargs)
        self.rate_limit_delay_seconds = self.rate_limit_delay_seconds
        self.logger = logging.getLogger(self.__class__.__name__) 
        self.logger.debug(
            f"Initialised {self.__class__.__name__} with API URL {self.url}"
        )

        self.page = None
        self.total_pages = None
        self.progress = None
        self.fields = []

    def get_json(self, url, headers={}, **data):
        for k, v in DEFAULT_HEADERS.items( ):
            headers.setdefault(k, v)
        self.logger.debug(f"Calling URL {url} with params {data}")
        self.logger.debug(f"Headers are {headers}")
        resl = requests.get(
            url, headers=headers, params=data
        )
        if resl.status_code == 200:
            self.rate_limit_delay_seconds = INITIAL_RATE_LIMIT
            try:
                js = resl.json( )
            except (ValueError, TypeError):
                self.logger.warn(
                    "Unable to decode JSON {resl.text}, returning empty."
                )
                return {}
            return js
        elif resl.status_code == 429:
            self.logger.warn(
                f"Rate limited by IGG, sleeping for "
                f"{self.rate_limit_delay_seconds}"
            )
            time.sleep(self.rate_limit_delay_seconds)
            self.rate_limit_delay_seconds *= 2
            self.logger.debug("Retrying IGG API URL.")
            return self.get_json(url, headers, **data)
        else:
            self.logger.error(
                f"Unknown response code {resl.status_code} from IGG. "
                "Bailing out."
            ) 
            self.logger.debug(f"Response body: {resl.text}")
            raise RuntimeError(
                f"Invalid response code {resl.status_code} from IGG API"
            )

    @classmethod
    def get_url_for_type(cls, type, **kwargs):
        try:
            return IGG_API_URLS[type].format(**kwargs)
        except KeyError:
            raise ValueError("Invalid URL type {}, choose one of {}".format(
                type, "' ".join(IGG_API_URLS.keys( ))
            ))

    def __iter__(self):
        raise NotImplementedError

    def __next__(self):
        raise NotImplementedError

    def _update_pagination_from_response(self, resp):
        if "pagination" not in resp:
            self.logger.warn(
                "Response does not contain pagination data. Assuming single "
                "page of data."
            )
            self.page = None
        else:
            p = resp["pagination"]
            if not p["next"]:
                # We have processed the last page
                self.page = None
                self.logger.debug("Last page has been processed.")
            else:
                self.page = p["next"]
                if p.get('pages'):
                    self.total_pages = p['pages']
                    self.progress = int((self.page / self.total_pages) * 100.0)
                else:
                    self.total_pages = 'Unknown'
                self.logger.debug(f"Pagination data is {p}")

    @property
    def all_data(self):
        data = []
        for chunk in self:
            data.extend(chunk)
        return data

class IGGPledgesScraper(IGGPageScraper):
    def __init__(
        self, campaign_id, fields=[], sort="natural",
        strip_zero_pledge=True, filter_amount=None
    ):
        super( ).__init__(type="pledges", campaign_id=campaign_id)
        self.progress = 0
        self.fields = fields or [
            "pledger_display_name", "display_amount",
            "display_amount_iso_code", "order_id", "timestamp"
        ]
        self.sort = sort
        self.strip_zero = strip_zero_pledge
        self.first = True
        self.filter_amount = filter_amount

    def __iter__(self):
        self.page = 1
        return self

    def __next__(self):
        if not self.page: raise StopIteration
        resp = self.get_json(
            self.url, page=self.page
        )
        self._update_pagination_from_response(resp)
        data = []
        for row in resp['response']:
            if self.strip_zero:
                if not row.get('display_amount'): continue

            da = INT_REGEX.sub('', row['display_amount'])
            try:
                row['amount'] = float(da)
            except:
                row['amount'] = 0

            if self.filter_amount:
                if row['amount'] < self.filter_amount: continue

            if self.first:
                self.logger.info(f"All possible fields are {list(row.keys())}")
                self.first = False

            try:
                ts = dateparser.parse(row['time_ago'])
                row['timestamp'] = ts.strftime("%Y-%m-%d %H:%M:%S")
            except Exception as ex:
                self.logger.debug(
                    f"Unable to parse 'time ago' {row['time_ago']}"
                )
                row['timestamp'] = row['time_ago']
        
            r = { }
            for fld in row.keys( ):
                r[fld] = row.get(fld, '') or ''
            data.append(r)
        if self.page and self.total_pages:
            self.logger.info(
                f"Processed page {self.page} of {self.total_pages}"
                f" ({self.progress}%)"
            )
        return data


    @property
    def all_data(self):
        data = super( ).all_data
        if self.sort == "natural":
            return list(reversed(data))
        elif self.sort:
            sort = self.sort
            r = False
            if sort[0] == "-":
                r = True
                sort = sort[1:]

            data.sort(key=lambda x: x.get(sort) or 0, reverse=r)
        return data

all_types = {
    "pledges": IGGPledgesScraper
}